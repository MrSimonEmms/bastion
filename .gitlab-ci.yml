include:
  - project: "mrsimonemms/gitlab-ci-tasks"
    ref: "master"
    file: "tasks/common.yml"

variables:
  DOCKER_BUILDX_VERSION: 0.4.2

docker_publish:
  stage: publish
  extends: .docker_base
  variables:
    DOCKER_PLATFORMS: linux/amd64,linux/arm/v7
  script:
    - export DOCKER_IMG_NAME=$(echo ${CI_REGISTRY}/${CI_PROJECT_PATH} | tr '[:upper:]' '[:lower:]')
    - export DOCKER_TAG="${CI_COMMIT_REF_SLUG}"
    - |
      if [ "${DOCKER_TAG}" == "master" ]; then
        DOCKER_TAG=latest
      fi
    - docker buildx build
      --platform ${DOCKER_PLATFORMS}
      --tag "${DOCKER_IMG_NAME}":"${DOCKER_TAG}"
      --push
      "${CI_PROJECT_DIR}"
    - |
      if [ -s VERSION ]; then
        echo "Setting DOCKER_VERSION as VERSION file"
        export DOCKER_VERSION=$(cat VERSION)
        echo $DOCKER_VERSION

        docker buildx build \
          --platform ${DOCKER_PLATFORMS} \
          --tag "${DOCKER_IMG_NAME}":"${DOCKER_VERSION}" \
          --push \
          "${CI_PROJECT_DIR}"
      else
        echo "Not setting DOCKER_VERSION"
      fi

update-version:
  extends: .repo_update

deploy_caddy:
  extends: .ssh_deploy_base
  stage: deploy
  variables:
    CONTAINER_NAME: caddy
  environment:
    name: production
    on_stop: destroy_caddy
  only:
    - master
  script:
    - export DOCKER_IMG_AND_VERSION="$(echo \"${DOCKER_IMG}:${VERSION}\" | tr '[:upper:]' '[:lower:]')"
    - |
      ssh ${SSH_REMOTE_NAME} << EOF
        docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
        docker pull ${DOCKER_IMG_AND_VERSION}
        docker stop ${CONTAINER_NAME} || true
        docker rm ${CONTAINER_NAME} || true
        echo "Deploying ${DOCKER_IMG_AND_VERSION}"
        docker run \
          --name "${CONTAINER_NAME}" \
          -d \
          -p 80:80 \
          -p 443:443 \
          -e CF_API_TOKEN=${CLOUDFLARE_API_TOKEN} \
          -v "\${PWD}/caddy/data:/data" \
          --restart="always" \
          ${DOCKER_IMG_AND_VERSION}
      EOF

deploy_pihole:
  extends: .ssh_deploy_base
  stage: deploy
  variables:
    BASTION_IP: 192.168.1.9
    CONTAINER_NAME: pihole
    DOCKER_IMG: pihole/pihole:2022.07.1 # @link https://github.com/pi-hole/docker-pi-hole/issues/1186
    DNS1: 1.1.1.1
    DNS2: 1.0.0.1
  environment:
    name: pihole
    on_stop: destroy_pihole
  only:
    - master
  script:
    - |
      ssh ${SSH_REMOTE_NAME} << EOF
        mkdir -p \${PWD}/etc-pihole
        mkdir -p \${PWD}/etc/dnsmasq.d
        docker pull ${DOCKER_IMG}
        docker stop ${CONTAINER_NAME} || true
        docker rm ${CONTAINER_NAME} || true
        echo "Deploying ${DOCKER_IMG}"
        docker run \
          -d \
          --restart=always \
          --name ${CONTAINER_NAME} \
          -p 53:53/tcp \
          -p 53:53/udp \
          -p 8080:80 \
          -p 8443:443 \
          -e TZ=UTC \
          -e WEBPASSWORD=${PIHOLE_PASSWORD} \
          -e DNS1=${DNS1} \
          -e DNS2=${DNS2} \
          -e ServerIP=${BASTION_IP} \
          -e VIRTUAL_HOST=${BASTION_IP}:8080 \
          --dns=127.0.0.1 \
          --dns=${DNS1} \
          -v \${PWD}/etc-pihole:/etc/pihole \
          -v \${PWD}/etc/dnsmasq.d:/etc/dnsmasq.d \
          ${DOCKER_IMG}
      EOF

destroy_caddy:
  extends: .ssh_deploy_base
  stage: destroy
  environment:
    name: production
    action: stop
  only:
    - master
  variables:
    GIT_STRATEGY: none
  when: manual
  script:
    - |
      ssh ${SSH_REMOTE_NAME} << EOF
        docker stop ${CONTAINER_NAME}
        docker rm ${CONTAINER_NAME}
      EOF

destroy_pihole:
  extends: .ssh_deploy_base
  stage: destroy
  environment:
    name: pihole
    action: stop
  only:
    - master
  variables:
    CONTAINER_NAME: pihole
    GIT_STRATEGY: none
  when: manual
  script:
    - |
      ssh ${SSH_REMOTE_NAME} << EOF
        docker stop ${CONTAINER_NAME}
        docker rm ${CONTAINER_NAME}
      EOF

#############################
# Extendable Configurations #
#############################

.docker_base:
  image: docker:stable
  services:
    - docker:dind
  tags:
    - docker
  before_script:
    - apk add --no-cache curl
    - mkdir -p /usr/lib/docker/cli-plugins
    - curl -L
      --output /usr/lib/docker/cli-plugins/docker-buildx
      "https://github.com/docker/buildx/releases/download/v${DOCKER_BUILDX_VERSION}/buildx-v${DOCKER_BUILDX_VERSION}.linux-amd64"
    - chmod a+x /usr/lib/docker/cli-plugins/docker-buildx
    - docker buildx version
    - docker buildx create --use
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    - docker run --rm --privileged multiarch/qemu-user-static --reset -p yes # Required for building multi-arch images
