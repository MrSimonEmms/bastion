# This is the riggerthegeek/caddy2-cloudflare image
#
# FROM caddy:2-builder AS builder
# RUN xcaddy build \
#   --with github.com/caddy-dns/cloudflare

FROM caddy:2-alpine
COPY --from=riggerthegeek/caddy2-cloudflare /usr/bin/caddy /usr/bin/caddy
COPY Caddyfile /etc/caddy/Caddyfile
